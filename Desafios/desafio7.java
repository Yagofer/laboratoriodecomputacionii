package desafios;

public class desafio7 {
	public static void main(String[] args) {
		
		//Funciones exponencial y logar�tmica
		//La funci�n exponencial exp devuelve el n�mero e elevado a una potencia
		
		 System.out.println("exp(1.0) es " +  Math.exp(1.0));
		    System.out.println("exp(10.0) es " + Math.exp(10.0));
		    System.out.println("exp(0.0) es " +  Math.exp(0.0));
		
		    //La funci�n log calcula el logaritmo natural (de base e) de un n�mero
		
		    System.out.println("log(1.0) es " + Math.log(1.0));
		    System.out.println("log(10.0) es " + Math.log(10.0));
		    System.out.println("log(Math.E) es " + Math.log(Math.E));
		
		
		    //Funciones trigonom�tricas
		    double angulo = 45.0 * Math.PI/180.0;
		    System.out.println("cos(" + angulo + ") es " + Math.cos(angulo));
		    System.out.println("sin(" + angulo + ") es " + Math.sin(angulo));
		    System.out.println("tan(" + angulo + ") es " + Math.tan(angulo));
		   // Para pasar de coordenadas rectangulares a polares es �til la funci�n atan2, que admite dos argumentos, 
		    //la ordenada y la abscisa del punto. Devuelve el �ngulo en radianes.

		    double y=-6.2;  //ordenada
		    double x=1.2;   //abscisa
		    System.out.println("atan2(" + y+" , "+x + ") es " + Math.atan2(y, x));
		    
		    //Funci�n potencia
		    //Para elevar un n�mero x a la potencia y, se emplea pow(x, y)

		       System.out.println("pow(10.0, 3.5) es " +  Math.pow(10.0,3.5));
		    
		    
		
	}
}
