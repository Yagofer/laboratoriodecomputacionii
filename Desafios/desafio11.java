package desafios;

import java.util.Random;


public class desafio11 {
	
          public static void main (String[] args) {
        	  
        	  
              CuentaCorriente cta1 = new CuentaCorriente ("Angie", 3000, null);
        	  CuentaCorriente cta2 = new CuentaCorriente ("Esteban", 2000, null);
        	  
        	  CuentaCorriente.Transferencia(cta1, cta2, 500);
        	  
        	  System.out.println(cta1.toString());
        	  System.out.println(cta2.toString());
          }
}

    class CuentaCorriente {
    	
	private double saldo;
	private String nombreTitular;
	private long numeroCuenta;
	
	
	public CuentaCorriente (String nombreTiular, double saldo, String nombreTitular) {
		this.nombreTitular = nombreTitular;
		this.saldo = saldo;
		
		Random aleatorio = new Random();
		
		this.numeroCuenta = Math.abs(aleatorio.nextLong());
	}
	
	
	public  String ingresarDinero (double dinero) {
		if (dinero>0) {
			this.saldo+=dinero;
			return "Se ha ingresado $ "+dinero;
		}else {
			return "no se puede inresar un valor negativo";
		}
	}
	
	public void sacarDinero(double dinero) {
		this.saldo-=dinero;
	}
	
	public double getSaldo() {
		return saldo;
	}

	public String toString() {
		return "cuentaCorriente [saldo=" + saldo + 
				", nombreTitular=" + nombreTitular +
				", numeroCuenta=" + numeroCuenta
				+ "]";
	}
	public static void Transferencia(CuentaCorriente ctaSalida, CuentaCorriente ctaEntrada, double dinero ) {
    ctaEntrada.saldo+=dinero;
    ctaSalida.saldo-=dinero;
	}
	
}    

