package desafios;

import javax.swing.JOptionPane;



public class desafio10 {

		public static void main(String[] args) {
			
			int adivinar = (int) ((Math.random() * 100)+1); // genera un n�mero entre 0 y 99
			int i = 0; // crea variable contador que es usada para contar los intentos fallidos
			int apuesta = input("Adivina el n�mero secreto entre 1 y 100 \n�Que n�mero es?");
			while (adivinar != apuesta) { // si el n�mero dado es distinto al sorteado repite
				i++; // incrementa variable contador
				if (adivinar > apuesta) {
					apuesta = input("El n�mero a adivinar es m�s grande. \n Intentelo otra vez");
				} else {
					apuesta = input("El n�mero a adivinar es m�s chico. \n Intentelo otra vez");
				}
			}
			JOptionPane.showMessageDialog(null,
					"CORRECTO! \nEl numero es:  "+adivinar+ "\nIntentos fallidos: " + i );
		}

		private static int input(String text) {
			return Integer.parseInt(JOptionPane.showInputDialog(text));
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	